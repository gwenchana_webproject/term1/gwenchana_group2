type Gender = 'male' | 'female' | 'others'
type Role = 'admin' | 'user'
type User = {
    id: number
    email: string
    password: string
    fullName: string
    gender: Gender // Male, Femail, Others
    roles: Role[] //admin, user
    workTime: number
    workRate: number
    date: string
    checkIN: string
    checkOUT: string
    isCheckedIn: boolean
    isCheckedOut: boolean
}
export type{Gender,Role,User}