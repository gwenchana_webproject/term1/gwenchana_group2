import type { Product } from "./Products"

type ReceiptItem = {
    id: number,
    name: string
    price: number
    unit: number
    productId: number
    product?: Product
}

export { type ReceiptItem }